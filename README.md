A consul held the highest elected political office of the Roman Republic (509 to 27 BC), and ancient Romans considered the consulship the highest level of the cursus honorum (an ascending sequence of public offices to which politicians aspired). Each year, the citizens of Rome elected two consuls to serve jointly for a one-year term. The consuls alternated in holding fasces each month when both were in Rome and a consul's imperium extended over Rome and all its provinces.

There were two consuls in order to create a check on the power of any individual citizen.

After the establishment of the Empire (27 BC), the consuls became mere symbolic representatives of Rome's republican heritage and held very little power and authority, with the Emperor acting as the supreme authority. 